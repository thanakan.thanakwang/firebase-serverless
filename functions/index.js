const functions = require('firebase-functions');
const admin = require('firebase-admin');
const uuid = require('uuid');
admin.initializeApp();

const db = admin.firestore();
const userRef = db.collection('user')

exports.CreateUser = functions.https.onRequest((request, response) => {
    //collection is similar to book and doc is it's own page like page 1 (doc 1)
    userRef.doc(uuid.v4()).set(request.body)
    response.sendStatus(201);
});

exports.GetUsers = functions.https.onRequest(async (request, response) =>{
    const doc = await userRef.get();

    //const test = (await userRef.get()).docs().map((user => ({ 'id': user.id ,...user.Data()}));

    if (!doc.exists) {
        const result = []
        
        doc.forEach(each => {
            const newData = Object.assign({"id": each.id}, each.data())
            //console.log(newData)
            result.push(newData)
        });
        response.status(200).send(result)
    } else {
        response.sendStatus(500);
    }
})